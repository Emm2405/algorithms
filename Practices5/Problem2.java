package Practices5;

import java.util.Scanner;

public class Problem2 {
    //1
    public static void reverseAnArray(int[] a, int left,int right){
        if(left >= right) {
            return;
        }
        int temp = a[left];
        a[left] = a[right];
        a[right] = temp;
        
        reverseAnArray(a, left + 1, right - 1);
    }
    
    //2
    public static int binarySearch(int a[], int x ,int left, int right) {
        if(left <= right) {
            int mid = (left + right) / 2;
            if(a[mid] == x) {
                return mid;
            }
            if(x > a[mid]) {
                return binarySearch(a, x, mid + 1, right);
            }
            else {
                return binarySearch(a, x, left, mid - 1);
            }
        }
        return -1;
    }
    
    //3
    public static void merge(int a[],int left, int mid, int right) {
        int n1 = mid - left + 1 ; 
        int n2 = right - mid;
        int[] L = new int[n1];
        int[] R = new int[n2];
        for(int i = 0; i < n1; i++) {
            L[i] = a[left + i];   
        }
        for(int j = 0; j < n2; j++) {
            R[j] = a[j + mid + 1];
        }
        int i = 0;
        int j = 0;
        int k = left;
        while(i < n1 && j < n2) {
            if(L[i] <= R[j]) {
                a[k] = L[i];
                k++;
                i++;
            }
            else {
                a[k] = R[j];
                k++;
                j++;
            } 
        }
        while(j < n2) {
            a[k] = R[j];
            k++;
            j++;
        }
        while(i < n1) {
            a[k] = L[i];
            k++;
            i++;
        }
    }
    
    public static void mergeSort(int a[], int left, int right) {
        if(left < right) {
            int mid = left + (right - left) / 2;
            mergeSort(a, left, mid);
            mergeSort(a, mid + 1, right);
            merge(a, left, mid, right);
        }  
    }
    public static int pratition(int a[], int low, int hight){
        int pivot = a[hight];
        int i = low - 1;
        for(int j = low; j < hight; j++) {
            if(a[j] <= pivot) {
                i++;
                int temp = a[j];
                a[j] = a[i];
                a[i] = temp;
            }
        }
        int temp = a[i + 1];
        a[i + 1] = a[hight] ;
        a[hight] = temp;
        return i + 1;
    }
    
    public static void quickSort(int a[], int low, int hight) {
        if(low < hight) {
            int pi = pratition(a, low, hight);
            quickSort(a, low, pi - 1);
            quickSort(a, pi + 1, hight);
        }
        
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhap kich thuoc mang: ");
        int n = sc.nextInt();
        int a[] = new int [n];
        System.out.println("Nhap gia tri mang: ");
        for(int i = 0; i < n ; i++) {
            a[i] = sc.nextInt();
        }
        int luaChon;
        do {
            System.out.println("\t\t------------------ MENU ------------------");
            System.out.println("\t\t1. Dao nguoc mang");
            System.out.println("\t\t2. Tim kiem nhi phan");
            System.out.println("\t\t3. Thuat toan Merge sort");
            System.out.println("\t\t4. Thuat toan Quick Sort");
            System.out.println("\t\t5. Hien thi mang");
            System.out.println("\t\t0. Thoat.");
            System.out.println("\t\t------------------ END ------------------\n\n");
            System.out.println("Nhap lua chon: ");
            luaChon = sc.nextInt();
            switch(luaChon) {
                case 1:
                    reverseAnArray(a, 0, n - 1);
                    break;
                case 2:{
                    System.out.println("Nhap gia tri can tim:");
                    int temp = sc.nextInt();
                    System.out.println(binarySearch(a, temp, 0, n - 1));
                    break;
                }
                case 3:{
                    mergeSort(a, 0, n - 1);
                    break;
                }
                case 4:{
                    quickSort(a, 0, n - 1);
                    break;
                }
                case 5:{
                    for(int i = 0 ; i < n; i++) {
                        System.out.print(a[i] + " ");
                    }
                    System.out.println();
                    break;
                }
                case 0:{
                    break;
                }
                default:
                    System.out.println("Vui long lua chon lai.");
            }
        }while(luaChon != 0);
        
    }
}
