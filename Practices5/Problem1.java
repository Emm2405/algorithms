package Practices5;

import java.util.Scanner;

public class Problem1 {
    public static int sumOfTwoNumber(int num1, int num2) {
        if(num2 == 0) {
            return num1;
        }
        return sumOfTwoNumber(num1 + 1, num2 - 1);
    } 
    public static int productOfTwoNumber(int num1, int num2) {
        if(num2 == 1) {
            return num1;
        }
        return num1 + productOfTwoNumber(num1, num2 - 1);
    }
    
    public static int powerOfTwoNumber(int num1, int num2) {
        if(num2 == 0) {
            return 1;
        }
        return num1 * powerOfTwoNumber(num1, num2 - 1);
    }
    
    public static int GCD(int num1, int num2) {
        if(num2 == 0) {
            return num1;
        }
        return GCD(num2, num1 % num2); 
        
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhap gia tri:");
        int num1 = sc.nextInt();
        int num2 = sc.nextInt();
        int luaChon;
        do {
            System.out.println("\n\n\t\t------------------ MENU ------------------");
            System.out.println("\t\t1. Tinh tong cua hai so");
            System.out.println("\t\t2. Tinh tich cua hai so");
            System.out.println("\t\t3. Tinh luy thua cua hai so");
            System.out.println("\t\t4. Tim GCD(Uoc chung lon nhat) cua hai so");
            System.out.println("\t\t0. Thoat.");
            System.out.println("\t\t------------------ END ------------------\n\n");
            System.out.print("Nhap lua chon: ");
            luaChon = sc.nextInt();
            switch (luaChon) {
                case 1:{
                    System.out.println("Tong: "+sumOfTwoNumber(num1, num2));
                    break;
                }
                case 2:{
                    System.out.println("Tich: "+productOfTwoNumber(num1, num2));
                    break;
                }
                case 3:{
                    System.out.println("Luy thua: "+powerOfTwoNumber(num1, num2));
                    break;
                }
                case 4: {
                    System.out.println("Uoc chung lon nhat: "+GCD(num1, num2));
                    break;
                }
                case 0:{
                    break;
                }
                default:
                    System.out.println("Vui long lua chon lai.");
            }     
        }while(luaChon != 0);
    }
}
