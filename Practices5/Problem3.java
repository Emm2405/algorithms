package Practices5;

import java.util.Scanner;

public class Problem3 {
    public static int Min(int a[], int n) {
       if(n == 1) {
           return a[0];
       }
       int min = a[n-1];
       if(Min(a, n-1) < min) {
           min = Min(a, n-1);
       }
       return min;
    }
    
    public static int Max(int a[], int n) {
        if(n == 1) {
            return a[0];
        }
        int max = a[n - 1];
        if(Max(a, n - 1) > max) {
            max = Max(a, n - 1);
        }
        return max;
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhap kich thuoc mang: ");
        int n = sc.nextInt();
        int a[] = new int [n];
        for(int i = 0; i < n ; i++) {
            a[i] = sc.nextInt();
        }
        System.out.println("min = "+Min(a, n)+", max = "+Max(a, n));
      
    }

}
