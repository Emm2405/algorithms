package Practices5;

import java.util.Scanner;

public class Problem4 {
    public static String removeAdjacentDuplicates(String str) {
        if (str.length() <= 1 ) {
          return str;
        }
        StringBuilder result = new StringBuilder();
        int i = 0;
        while(i < str.length()) {
            int count = 1;
            char currentChar = str.charAt(i);
            while(i + count < str.length() && str.charAt(i + count) == currentChar) {
                count++;
            }
            if(count > 1) {
                i += count;
            }
            else {
                result.append(currentChar);
                i++;
            }
        }
        String newResutl = result.toString();
        if(newResutl.equals(str)) {
            return newResutl;
        }
        else {
            return removeAdjacentDuplicates(newResutl);
        }
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.next();
        String result = removeAdjacentDuplicates(str);
        if(result.isEmpty()) {
            System.out.println("Empty String");
        }
        else {
            System.out.println(result);
        }
    }
}
