package Practices2;

import java.util.Scanner;

public class Problem1 {
	private int top;
	private int a[];
	private int size;
	
	public Problem1(int size) {
		this.size = size; 
		a = new int[size];
		this.top = -1;
	}
	
	public int size() {
		return top + 1;
	}
	
	public boolean isEmpty() {
		return top == -1;
	}
	
	public int top() {
		if(isEmpty()) {
			System.out.println("Stack rong!");
			return -1;
		}
		else {
			return a[top];
		}

		
	}
	
	public void push(int element) {//them cuoi
		if(top >= a.length - 1) {
			System.out.println("Stack day!");
			return;
		}
		++top;
		a[top] = element;
		
	}
	public void pop() {//xoa cuoi
		if(isEmpty()) {
			System.out.println("Stack rong!");
			return;
		}
		--top;
	}
	
	
	
	public static void main(String[] args) {
		Problem1 Stack = new Problem1(5);
		Scanner sc = new Scanner(System.in);
	
		int luachon;
		do {
			System.out.println("\n\n\t\t------------------ MENU ------------------");
			System.out.println("\t\t1. Do dai cua stack");
			System.out.println("\t\t2. Kiem tra rong");
			System.out.println("\t\t3. Gia tri cuoi cung cua stack");
			System.out.println("\t\t4. Them gia tri vao stack");
			System.out.println("\t\t5. Xoa gia tri cuoi cua stack");
			System.out.println("\t\t0. Thoat.");
			System.out.println("\t\t------------------ END ------------------\n\n");
			System.out.print("Nhap lua chon: ");
			luachon = sc.nextInt();
			
			switch(luachon) {
				case 1: 
					System.out.println();
					System.out.println("Do dai cua stack: "+Stack.size());
					break;
				case 2:
					System.out.println();
					System.out.println(Stack.isEmpty());
					break;
				case 3:
					System.out.println();
					System.out.println("Gia tri cuoi cung cua stack: "+Stack.top());
					break;
				case 4:
					System.out.println("Nhap gia tri can them vao stack: ");
					int temp = sc.nextInt();
					Stack.push(temp);
					System.out.println();
					System.out.println("Da them thanh cong "+ temp+" vao stack");
					break;
				case 5:
					System.out.println();
					Stack.pop();
					System.out.println("Da xoa gia tri cuoi cung.");
				case 0: 
					break;
				default:
					System.out.println("Vui long lua chon lai.");
			}
		}while(luachon!=0);
	}

}
