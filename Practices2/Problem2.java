package Practices2;

import java.util.Scanner;

public class Problem2 {
	private int size;
	private int a[];
	private int left;
	private int right;
	
	public Problem2(int size) {
		this.size = size;
		this.a = new int [size];
		this.left = 0;
		this.right = -1;
	}
	
	public int size() {
		if(left <= right) {
			return right - left + 1;
		}
		else {
			return 0;
		}
	}
	
	public boolean isEmpty() {
		return size() == 0;
	}
	
	public int front() {
		if(isEmpty()) {
			System.out.println("Queue rong!");
			return -1;
		}
		return a[left];
	}
	
	public int back() {
		if(isEmpty()) {
			System.out.println("Queue rong!");
			return -1;
		}
		return a[right];
	}
	
	public void push(int element) {
		if(right >= a.length - 1) {
			System.out.println("Queue day!");
			return;
		}
		++right;
		a[right] = element;
	}
	
	public void pop() {
		if(isEmpty()) {
			System.out.println("Queue rong!");
			return;
		}
		++left;
	}
	
	public static void main(String[] args) {
		Problem2 Queue = new Problem2(5);
		
		Scanner sc = new Scanner(System.in);
		
		int luachon;
		do {
			System.out.println("\n\n\t\t------------------ MENU ------------------");
			System.out.println("\t\t1. Do dai cua queue");
			System.out.println("\t\t2. Kiem tra rong");
			System.out.println("\t\t3. Gia tri dau cua queue");
			System.out.println("\t\t4. Gia tri cuoi cua queue");
			System.out.println("\t\t5. Them gia tri vao queue");
			System.out.println("\t\t6. Xoa gia tri cuoi cua queue");
			System.out.println("\t\t0. Thoat.");
			System.out.println("\t\t------------------ END ------------------\n\n");
			System.out.print("Nhap lua chon: ");
			luachon = sc.nextInt();
			
			switch(luachon) {
				case 1: 
					System.out.println("Do dai cua queue: "+Queue.size());
					break;
				case 2:
					System.out.println(Queue.isEmpty());
					break;
				case 3:
					System.out.println("Gia tri dau cua queue: "+Queue.front());
					break;
				case 4:
					System.out.println("Gia tri cuoi cua queue: "+Queue.back());
					break;
				case 5:
					System.out.print("Nhap gia tri can them vao queue: ");
					int temp = sc.nextInt();
					Queue.push(temp);
					System.out.println("Da them thanh cong "+ temp+" vao queue");
					break;
				case 6:
					Queue.pop();
					break;
				case 0: 
					break;
				default:
					System.out.println("Vui long lua chon lai.");
			}
		}while(luachon!=0);
		
	}

}
