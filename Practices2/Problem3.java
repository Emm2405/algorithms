package Practices2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

import javax.management.QueryEval;

public class Problem3 {
	static class Point{
		private int x;
		private int y;
		public Point(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}
	static int dx[] = {1, -1, 0, 0};
	static int dy[] = {0, 0, 1, -1};
	
	public static int dayRottenApple(int a[][], int n, int m) {
		Queue<Point> TaoHu = new LinkedList<Problem3.Point>();
		
		for(int i = 0; i < n; i++) {
			for(int j = 0; j  < m; j++) {
				if(a[i][j] == 2) {
					TaoHu.add(new Point(i, j));
				}
			}
		}
		int ngay = 0;
		while(!TaoHu.isEmpty()) {
			int size = TaoHu.size();
			boolean changed = false;
			for(int i = 0; i < size; i++) {
				Point current = TaoHu.poll();
				int x = current.x;
				int y = current.y;
				
				for(int j = 0; j < 4; j++) {
					int nx = x + dx[j];
					int ny = y + dy[j];
					if(nx >= 0 && nx < n && ny >= 0 && ny < m && a[nx][ny] == 1 ) {
						a[nx][ny] = 2;
						TaoHu.add(new Point(nx, ny));
						changed = true;
					}
				}
			}
			if (changed) {
				ngay++; 
			}
		}
		
		for (int b = 0; b < n; b++) {
		    for (int c = 0; c < m; c++) {
		        if (a[b][c] == 1) {
		        	return -1;
		        }
		    }
		}
		
		return ngay;
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Nhap so luong test case: ");
		int T = sc.nextInt();
		int a[][];
		int n,m;
		Problem2 queue = new Problem2(T);
		for(int tc = 0; tc < T; tc++) {
			System.out.print("Nhap so luong hang: ");
			n = sc.nextInt();
			System.out.print("Nhap so luong cot: ");
			m = sc.nextInt();
			a = new int [n][m];
			for(int i = 0; i < n; i++) {
				for(int j = 0; j < m; j++) {
					a[i][j] = sc.nextInt(); // nhap
				}
			}
			
			for(int i = 0; i < n; i++) {
				for(int j = 0; j < m; j++) {
					System.out.print(" "+a[i][j]); // xuat
				}
				System.out.println();
			}
			queue.push(dayRottenApple(a,n,m));
		}
		int size = queue.size();
		for(int i = 0; i < size; i++) {
			System.out.println(queue.front());
			queue.pop();
		}
		sc.close();
	}
}
