package Praticies7;

import java.util.Scanner;

public class Problem1 {
    public static void swap(int a[], int i, int j) {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }
    public static void selectionSort(int s[],int f[], int n) {
        for(int i = 0; i < n - 1; i++) {
            int min = i;
            for(int j = i + 1; j < n; j++) {
                if(f[j] < f[min]) {
                    min = j;
                }
            }
            swap(f, i, min);
            swap(s, i, min);
        }
    }
    public static void activitySelection(int s[],int f[], int n) {
        selectionSort(s, f, n);
        int i = 0, j = 0;
        int kq[] = new int [n];
        System.out.print(i + " ");
        while(i < n) {
            if(s[i] >= f[j]) {
                System.out.print(i+ " ");
                i++;
                j++;
            }
            else {
                i++;
            }
        }
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhap kich thuoc mang: ");
        int n = sc.nextInt();
        
        int s[] = new int [n];
        int f[] = new int [n];
        
        System.out.println("Nhap thoi gian bat dau cua cac hoat dong:");
        for(int i = 0; i < n; i++) {
            s[i] = sc.nextInt();
        }
        System.out.println("Nhap thoi gian ket thuc cua cac hoat dong:");
        for(int i = 0; i < n; i++) {
            f[i] = sc.nextInt();
        }
        
        activitySelection(s, f, n);
    }
}
