package Practices3;

import java.util.Scanner;

public class Problem2 {
	public static int[] firstAndLast(int a[], int n, int x) {
		int fas[] = new int[2];
		fas[0] = first(a, n, x);
		fas[1] = last(a, n, x);
		return fas;
	}
	public static int first(int a[], int n, int x) {
		int left = 0, right = n-1;
		int kq = -1;
		while(left <= right) {
			int chot = (left + right) / 2;
			if(a[chot] == x && (chot == 0||a[chot - 1] != x)) {
				kq = chot;
				break;
			}
			if(x > a[chot]) {
				left = chot + 1;
			}
			else {
				right = chot - 1;
			}
		}
		return kq;
	}
	public static int last(int a[], int n, int x) {
		int left = 0; int right = n - 1;
		int kq = -1;
		while(left <= right) {
			int chot =(left  + right) / 2;
			if(a[chot] == x && (chot == n-1||a[chot + 1] != x)) {
				kq = chot;
				break;
			}
			if(x >= a[chot]) {
				left = chot + 1;
			}
			else {
				right = chot - 1;
			}
		}
		return kq;
	}
	public static void main(String[] args) {
		int n;
		Scanner sc = new Scanner(System.in);
		System.out.print("Nhap kich thuoc mang: ");
		n = sc.nextInt();
		int a[] = new int [n];
		for(int i = 0; i < n; i++) {
			a[i] = sc.nextInt();
		}
		System.out.print("Nhap gia tri can tim kiem: ");
		int x = sc.nextInt();
		int kq[] = new int[2];
		kq = firstAndLast(a, n, x);
		System.out.println("["+kq[0] +","+kq[1]+"]");
	}
}
