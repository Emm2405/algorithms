package Practices3;

import java.util.Scanner;

public class Problem1 {

	public static int linearSearchAlgorithm(int a[], int n, int x) {
		for(int i = 0; i < n; i++) {
			if(a[i] == x) {
				return i;
			}
		}
		return -1;
	}
	
	public static int binarySearch(int a[], int n, int x) {
		int left = 0;
		int right = n-1;
		
		while(left <= right) {
			int chot = (left + right) / 2;
				if(a[chot] == x) {
					return chot;
				}
				
				if (x > a[chot]) {
		            left = chot + 1;
				}
		        else {
		            right = chot - 1;
		        }
		}
		return -1;
	}
	
	public static void main(String[] args) {
		int n;
		Scanner sc = new Scanner(System.in);
		System.out.print("Nhap kich thuoc mang: ");
		n = sc.nextInt();
		int a[] = new int [n];
		for(int i = 0; i < n; i++) {
			a[i] = sc.nextInt();
		}
		System.out.print("Nhap gia tri can tim kiem: ");
		int x = sc.nextInt();
//		if(linearSearchAlgorithm(a, n, x) == -1) {
//			System.out.println("Khong tim thay "+x+" trong mang");
//		}
//		else {
//			System.out.println("Vi tri cua "+x+" trong mang = "+linearSearchAlgorithm(a, n, x));
//		}
		if(binarySearch(a, n, x) == -1) {
			System.out.println("Khong tim thay "+x+" trong mang");
		}
		else {
			System.out.println("Vi tri cua "+x+" trong mang = "+binarySearch(a, n, x));
		}

		
	}

}
