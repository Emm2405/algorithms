package Practices3;

import java.util.Scanner;

public class Problem4 {

	public static int employeeRating(int a[], int n) {
		int count = 0;
		int max = 0;
		for(int i = 0 ; i < n ; i++) {
			if(a[i] >= 6  ) {
				count++;
			}
			else {
				max = max > count ? max : count;
				count = 0;
			}
		}
		max = count > max ? count : max;
		return max;
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Nhap so ngay lam viec: ");
		int n = sc.nextInt();
		int a[] = new int[n];
		for(int i = 0; i < n; i++) {
			a[i] = sc.nextInt();
		}
		System.out.println(employeeRating(a, n));
	}

}
