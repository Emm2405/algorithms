package Practices3;

import java.util.Scanner;

public class Problem3 {

	public static int findTheIndex(int a[], int n, int x) {
		int left = 0, right = n-1;
		int index = 0;
		while(left <= right) {
			int chot = (left + right) /2;
			if(a[chot] == x) {
				return chot;
			}
			else if(a[chot] < x) {
				left = chot + 1;
			}
			else {
				right = chot - 1;
			}
		}
		return left;
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Nhap kich thuoc cua mang: ");
		int n = sc.nextInt();
		int a[] = new int [n];
		for(int i = 0; i < n; i++) {
			a[i] = sc.nextInt();
		}
		System.out.print("Number: ");
		int key = sc.nextInt();
		System.out.println("Answer: "+findTheIndex(a, n, key));
	}
	
}
