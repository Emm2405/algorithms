package Practices4;

import java.util.Scanner;

public class Problem2 {
	public static void InsertionSort(int a[], int n) {
		for(int i = 1; i < n; i++) {
			int key = a[i];
			int j = i - 1;
			while(j >= 0 && a[j] > key) {
				a[j + 1] = a[j];
				j--;
			}
			a[j+1] = key;
		}
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Nhap kich thuoc mang: ");
		int n = sc.nextInt();
		int a[] = new int [n];
		for(int i = 0; i < n ; i++) {
			a[i] = sc.nextInt();
		}
		InsertionSort(a, n);
		for(int i = 0; i < n; i++) {
			System.out.print(a[i]+ " ");
		}
	}
}
