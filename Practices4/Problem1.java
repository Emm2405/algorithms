package Practices4;

import java.util.Scanner;

public class Problem1 {
	public static void selectionSort(int a[], int n) {
		for(int i = 0; i < n - 1; i++) {
			int min = i;
			for(int j = i + 1; j < n; j++) {
				if(a[j] < a[min]) {
					min = j;
				}
			}
			int temp = a[i];
			a[i] = a[min];
			a[min] = temp;
		}
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Nhap kich thuoc mang: ");
		int n = sc.nextInt();
		int a[] = new int [n];
		for(int i = 0; i < n; i++) {
			a[i] = sc.nextInt();
		}
		selectionSort(a, n);
		for(int i = 0; i < n; i++) {
			System.out.print(a[i] + " ");
		}
	}
}
