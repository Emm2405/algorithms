package Practices4;

import java.util.Scanner;

public class Problem6 {
    static int min;
    static int max;
    
    public static void maxMin(int a[], int n) {
        for(int i = 1; i < n; i++) {
            if(a[i] < min) {
                min = a[i];
            }
            if(a[i] > max) {
                max = a[i];
            }
        }
    }
    
    public static void bingoSort(int a[], int n) {
        min = a[0];
        max = a[0];
        maxMin(a, n);
        int largestEle = max;
        int nextElePos = 0;
        while(min < max) {
            int starPos = nextElePos;
            for(int i = starPos; i < n; i++) {
                if(a[i] == min) {
                    swap(a, i, nextElePos);
                    nextElePos++;
                }
                else if(a[i] < max) {
                    max = a[i];
                }
            }
            min = max;
            max = largestEle;
        }
    }
    
    private static void swap(int[] a, int i, int j) {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhap kich thuoc mang: ");
        int n = sc.nextInt();
        int a[] = new int [n];
        for(int i = 0; i < n ; i++) {
            a[i] = sc.nextInt();
        }
        
        bingoSort(a,n);
        
        for(int i = 0; i < n; i++) {
            System.out.print(a[i]+ " ");
        }
    }
}
