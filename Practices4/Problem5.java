package Practices4;

import java.util.Scanner;

public class Problem5 {
    private static int RUN = 32;
    /**
     * 
     * @param a
     * @param left
     * @param right
     */
    public static void insertSort(int a[], int left, int right) {
        for(int i = left + 1; i <= right; i++) {
            int j = i - 1;
            int key = a[i];
            while(j >= left && a[j] > key) {
                a[j + 1] = a[j];
                j--;
            }
            a[j + 1] = key;
        }
    }
    /**
     * 
     * @param a
     * @param l
     * @param m
     * @param r
     */
    public static void mergeSort(int a[], int l, int m, int r) {
        int n1 = m - l + 1;
        int n2 = r - m;
        int[] L = new int [n1];
        int[] R = new int [n2];
        for(int i = 0; i < n1; i++) {
            L[i] = a[l + i];
        }
        for(int j = 0; j < n2; j++) {
            R[j] = a[m + 1 + j];
        }
        int k = l;
        int i = 0;
        int j = 0;
        while(i < n1 && j < n2) {
            if(L[i] >= R[j]) {
                a[k] = L[i];
                i++;
                k++;
            }
            else {
                a[k] = R[j];
                j++;
                k++;
            }
        }
        while(i < n1) {
            a[k] = L[i];
            i++;
            k++;
        }
        while(j < n2) {
            a[k] = R[j];
            j++;
            k++;
        }
    }
    /**
     * 
     * @param arr
     * @param n
     */
    public static void timSort(int[] arr, int n) {
        for (int i = 0; i < n; i += RUN) {
            insertSort(arr, i, Math.min((i + 31), (n - 1)));
        }

        for (int size = RUN; size < n; size = 2 * size) {
            for (int left = 0; left < n; left += 2 * size) {
                int mid = left + size - 1;
                int right = Math.min((left + 2 * size - 1), (n - 1));

                mergeSort(arr, left, mid, right);
            }
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhap kich thuoc mang: ");
        int n = sc.nextInt();
        int a[] = new int [n];
        for(int i = 0; i < n ; i++) {
            a[i] = sc.nextInt();
        }
        timSort(a, n);
        for(int i = 0; i < n; i++) {
            System.out.print(a[i]+ " ");
        }
    }
}
