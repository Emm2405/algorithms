package Practices4;

import java.util.Scanner;

public class Problem3 {
	public static void countingSort(int a[], int n) {
		int max = a[0];
		for(int i = 0; i < n; i++) {
			if(a[i] > max) {
				max = a[i];
			}
		}
		int dem[] = new int [max + 1];
		for (int i = 0; i < n; i++) {
			dem[a[i]]++;
		}
		
		int i = 0, j = 0;
		while(i < dem.length) {
			if(dem[i] > 0) {
				a[j] = i;
				j++;
				dem[i]--;
			}
			else {
				i++;
			}
		}
}

	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Nhap kich thuoc mang: ");
		int n = sc.nextInt();
		int a[] = new int [n];
		for(int i = 0; i < n ; i++) {
			a[i] = sc.nextInt();
		}
		countingSort(a, n);
		System.out.println();
		for(int i = 0; i < n; i++) {
			System.out.print(a[i]+ " ");
		}
	}
}
