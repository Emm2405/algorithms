package Practices6;

import java.util.Scanner;

public class Problem1 {
    public static int binarySearchDAC(int a[],int x, int left, int right) {
        int mid = left + (right - left) / 2;
        if(a[mid] == x ) {
            return mid;
        }
        else if(x > a[mid] && mid < right) {
            return binarySearchDAC(a, x, mid + 1, right);
        }
        else if(x < a[mid] && mid > left){
            return binarySearchDAC(a, x, left, mid - 1);
        }

        return -1;
    }
    
    public static void merge(int a[], int left, int mid, int right) {
        int n1 = mid - left + 1;
        int n2 = right - mid;
        int l[] = new int [n1];
        int r[] = new int [n2];
        for(int i = 0; i < n1; i++) {
            l[i] = a[i + left];
        }
        for(int i = 0; i < n2; i++) {
            r[i] = a[i + mid + 1];
        }
        int i = 0;
        int j = 0;
        int k = left;
        while(i < n1 && j < n2) {
            if(l[i] <= r[j]) {
                a[k] = l[i];
                k++;
                i++;
            }
            else{
                a[k] = r[j];
                k++;
                j++;
            }
        }
        
        while(i < n1) {
            a[k] = l[i];
            i++;
            k++;
        }
        while(j < n2) {
            a[k] = r[j];
            j++;
            k++;
        }
    }
    
    public static void mergeSort(int a[], int left, int right) {
        if(left < right) {
            int mid = left + (right - left) / 2;
            mergeSort(a, left, mid);
            mergeSort(a, mid + 1, right);
            merge(a, left, mid, right);
        }
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhap kich thuoc mang: ");
        int n = sc.nextInt();
        int a[] = new int [n];
        for(int i = 0; i < n; i++) {
            a[i] = sc.nextInt();
        }
//        System.out.print("Nhap gia tri can tim: \n");
//        int x = sc.nextInt();
//        System.out.println(binarySearchDAC(a, x, 0, n - 1));
        mergeSort(a, 0, n - 1);
        for(int i = 0; i < n; i++) {
            System.out.print(a[i] + " ");
        }
    }

}
